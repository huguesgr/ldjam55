extends Button
var descr
var qty
var price
var generation
var type
var increment_call
signal event

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	update_self()

func buyable():
	for c in price:
		if Global.get(c) >= price[c]:
			pass
		else:
			return false
	return true

func init(container, invocation):
	text = invocation["name"]
	descr = invocation["name"]
	price = invocation["price"]
	generation = invocation["generation"]
	increment_call = invocation.get("call", false)
	qty = 0
	container.add_child(self)

func increment():
	qty += 1
	if increment_call:
		increment_call.call()

	# random event
	for e in Global.events:
		if (randi() % 100) < e.probability:
			emit_signal("event", e)
			break

func update_self():
	var description = descr + ' [' + str(qty) + ']\n'
	var cost = 'Cost: '
	for color in price:
		cost += str(price[color]) + Global.color_symbols[color]
	cost += '\n'
	var generate = ''
	if generation != {}:
		generate += 'Generates: '
		for g in generation:
			generate += str(generation[g]) + Global.color_symbols[g] + '(' + str(qty * generation[g]) + ')'
	text = description + cost + generate
	self.disabled = not buyable()

func _on_generation_timer_timeout():
	for g in generation:
		Global.increment_resource(g, qty * generation[g])
		self.disabled = not buyable()
