extends Node2D
var invoker_button = preload("res://src/invokerButton.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	for color in Global.colors:
		Global.set(color, 0)
		get_node("InvocationCircle/" + color.capitalize() + "Label").set_text(str(Global.get(color)))	
	Global.play_time = 0
	init_buttons()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_white_button_pressed():
	Global.increment_resource("white", 1)
	#button_await("white", 1)

func _on_blue_button_pressed():
	Global.increment_resource("blue", 1)
	#button_await("blue", 1)

func _on_black_button_pressed():
	Global.increment_resource("black", 1)
	#button_await("black", 1)

func _on_red_button_pressed():
	Global.increment_resource("red", 1)
	#button_await("red", 1)

func _on_green_button_pressed():
	Global.increment_resource("green", 1)
	#button_await("green", 1)

func init_buttons():
	for invocation in Global.invocations:
		add_invoker_button(invocation)
	#for action in Global.actions:
	#	add_action_button(action)

func add_invoker_button(invocation):
	var buttons_container = get_node("BuyMenu/InvocationMenu")
	var button = invoker_button.instantiate()
	button.init(buttons_container, invocation)
	button.pressed.connect(self._invoker_button_pressed.bind(button))
	button.connect("event", self._event)

func _event(event):
	var console_text = get_node("Console").text
	if len(console_text.split('\n')) <= 5:
		get_node("Console").text += "\nA terrible event happened, known as " + event.name
	else:
		console_text = '\n'.join(console_text.split('\n').slice(0, 4))
		console_text += "\nA terrible event happened, known as " + event.name
		get_node("Console").text = console_text
	pay(event)

func add_action_button(action):
	var buttons_container = get_node("BuyMenu/ActionMenu")
	var button = Button.new()
	button.text = action["name"]
	buttons_container.add_child(button)
	button.pressed.connect(action["call"])

func _invoker_button_pressed(button: Button):
	buy(button)

func button_await(color, timer):
	get_node("InvocationCircle/" + color.capitalize()  + "Button").disabled = true
	await get_tree().create_timer(timer).timeout
	get_node("InvocationCircle/" + color.capitalize()  + "Button").disabled = false

func buy(button):
	if button.buyable():
		for c in button.price:
			Global.increment_resource(c, -button.price[c])
		button.increment()
		return true
	return false

func pay(event):
	for p in event.price:
		Global.increment_resource(p, -event.price[p])

func _on_console_timer_timeout():
	var console_text = get_node("Console").text.split('\n')
	get_node("Console").text = "\n".join(console_text.slice(1, len(console_text)))

func _on_play_time_timeout():
	Global.play_time += 1

func _on_start_dialog_closed():
	$Music.play()

func _on_audio_button_toggled(toggled_on):
	if toggled_on:
		$Music.play()
	else:
		$Music.stop()
