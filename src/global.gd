extends Node
var endgame_scene = preload("res://src/endGame.tscn").instantiate()

var play_time

var white
var blue
var black
var red
var green

var color_symbols = {
	"white": "🌞",
	"blue": "💧",
	"black": "☠",
	"red": "🔥",
	"green": "🌳"
}
var colors = color_symbols.keys()

func endgame():
	get_tree().change_scene_to_file("res://src/endGame.tscn")
	print("GAME OVER")

func sacrifice():
	print("SACRIFICE")

var invocations = [
	{
		"name": "fire-frog",
		"type": ["red"],
		"generation": {"red": 2},
		"price": {"black": 5, "green": 8}
	}, {
		"name": "fish-slime",
		"type":  ["blue"],
		"generation": {"blue": 3},
		"price": {"white": 13, "black": 15}
	}, {
		"name": "angry-moss",
		"type": ["green"],
		"generation": {"green": 5},
		"price": {"white": 17, "red": 5}
	}, {
		"name": "lively-wisp",
		"type": ["white"],
		"generation": {"white": 7},
		"price": {"blue": 8, "green": 20}
	}, {
		"name": "swampy-drake",
		"type": ["black"],
		"generation": {"black": 9},
		"price": {"blue": 12, "red": 25}
	}, {
		"name": "world-ender",
		"type": ["black"],
		"generation": {},
		"price": {
			"white": 9999,
			"blue": 9999,
			"black": 9999,
			"red": 9999,
			"green": 9999
		},
		"call": endgame
	}
]

var actions = [
	{
		"name": "Sacrifice invocations",
		"call": sacrifice
	}
]

# probability: 0 to 100
var events = [
	{
		"probability": 9,
		"name": "The Dragon Awakening",
		"price": {
			"white": 15,
			"blue": 20,
			"black": 25,
			"red": 20,
			"green": 15
		}
	},
	{
		"probability": 6,
		"name": "The Elven Coronation",
		"price": {
			"white": 10,
			"blue": 15,
			"black": 15,
			"red": 15,
			"green": 10
		}
	},
	{
		"probability": 8,
		"name": "The Lost City Discovery",
		"price": {
			"white": 20,
			"blue": 20,
			"black": 20,
			"red": 20,
			"green": 20
		}
	},
	{
		"probability": 4,
		"name": "The Dark Sorcerer's Return",
		"price": {
			"white": 10,
			"blue": 10,
			"black": 30,
			"red": 10,
			"green": 10
		}
	},
	{
		"probability": 8,
		"name": "The Enchanted Forest Revival",
		"price": {
			"white": 15,
			"blue": 10,
			"black": 10,
			"red": 10,
			"green": 15
		}
	}
]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	
func increment_resource(resource_name, value):
	Global.set(resource_name, max(0, Global.get(resource_name) + value))
	get_node("../Map/InvocationCircle/" + resource_name.capitalize() + "Label").set_text(str(Global.get(resource_name)))

func remove_mana(amount):
	if amount <= 0:
		return
	var color_amounts = {}
	for color in Global.colors:
		if Global.get(color) > 0:
			color_amounts[color] = Global.get(color)
	var min_amount = color_amounts.values().min()
	var n_colors = color_amounts.size()
	var to_remove = min(min_amount, amount / n_colors)
	for color in color_amounts:
		increment_resource(color, -1 * to_remove)
	if (to_remove * n_colors) < amount:
		remove_mana(amount - to_remove * n_colors)
