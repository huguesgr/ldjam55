extends Button

var scrolls = [
	{
		"goal": 500,
		"unlock": "../SacrificeButton"
	}
]
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if scrolls == []:
		text = "No research available"
		disabled = true
	else:
		var scroll = scrolls[0]
		text = "Next research (" + str(scroll["goal"]) + " mana)"
		var total_mana = 0
		for color in Global.colors:
			total_mana += Global.get(color)
		if total_mana > scroll["goal"]:
			disabled = false
		else:
			disabled = true

func _on_pressed():
	var scroll = scrolls.pop_at(0)
	get_node(scroll["unlock"]).show()
	Global.remove_mana(scroll["goal"])
