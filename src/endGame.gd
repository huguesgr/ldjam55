extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("../Map").free()
	$PlayTime.text = "Score : " + str( round(Global.play_time/60)) + " min "  + str(round(Global.play_time%60)) + "sec"

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
