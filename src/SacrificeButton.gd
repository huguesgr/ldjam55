extends Button


# Called when the node enters the scene tree for the first time.
func _ready():
	hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var invocations = $"../../InvocationMenu".get_children()
	var invocations_count = 0
	for invocation in invocations:
		invocations_count += invocation.qty
	text = "Sacrifice invocations (" + str(invocations_count) + ")"
	if invocations_count == 0:
		disabled = true
	else:
		disabled = false

func _on_pressed():
	var total_mana = 0
	for invocation in $"../../InvocationMenu".get_children():
		for ressource in invocation.price.values():
			total_mana += invocation.qty * ressource
		invocation.qty = 0
	var rand_colors = {}
	var total_rand = 0
	for color in Global.colors:
		rand_colors[color] = randf()
		total_rand += rand_colors[color]
	var luck = randf() * 0.5 + 1
	for color in Global.colors:
		Global.set(color, Global.get(color) + ceil(total_mana * luck * (rand_colors[color] / total_rand)))
