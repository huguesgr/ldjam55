extends Node
var map_scene = preload("res://src/map.tscn").instantiate()

func _ready():
	new_game()

func new_game():
	get_tree().root.add_child.call_deferred(map_scene)

func _process(_delta):
	pass
